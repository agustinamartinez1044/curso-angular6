export class DestinoViaje {
    nombre: string;
    imagenUrl: string;
    descripcion: string;

    constructor(nomb: string, url: string, desc: string) {

        this.nombre = nomb;
        this.imagenUrl = url;
        this.descripcion = desc;

    }
}
